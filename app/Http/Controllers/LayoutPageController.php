<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\index;
class LayoutPageController extends Controller
{
    public function layout()
    {
        return view('layout');
    }
}